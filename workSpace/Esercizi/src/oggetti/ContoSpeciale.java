package oggetti;

public class ContoSpeciale extends Contocorrente {

	private final double limite =200;
	
	public void preleva(double importo){
		
		if(importo<=limite){
			
			if(importo<=getSaldo())
				
				super.preleva(importo);
			else 
				System.out.println("Prelevamento non disponibile, barbone.");
		}
			else 
				System.out.println("Prelevamento rifiutato, spendaccione.");
	}
	
	public void versa(Assegno p){
		
		super.versa(p.importo);
	}
}
