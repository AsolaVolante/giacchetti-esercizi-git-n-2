package redgui.ActionListenerInterface.src;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class RedGUI4  {
	
	private JFrame frame;

	public RedGUI4() {
		super();
		frame = new JFrame("project ActionListenerInterface");
		JButton button = new JButton("OK");
		frame.add(button, BorderLayout.SOUTH);
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(Color.RED);
				
			}
		});
		frame.setVisible(true);

	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new RedGUI4();
				
			}
		});
	}

}