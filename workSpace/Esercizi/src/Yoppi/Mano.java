package Yoppi;

public class Mano {
	private Carta[] m;
	public Mano(int nCarte,MazzoDiCarte mazzo)
	{
		m = new Carta[nCarte];
		for(int i=0;i<3;i++) m[i] = mazzo.pescaCarta();
	}
	public void setNCarte(int nCarte)
	{
		m = null;
		m = new Carta[nCarte];
	}
	public int getNCarte()
	{
		return m.length;
	}
	public Carta[] getM() {
		return m;
	}
	public void setM(Carta[] m) {
		this.m = m;
	}
	public void scartaCarta(int carta)
	{
		m[carta] = null;
		if(carta==0)
		{
			m[0] = m[1];
			m[1] = m[2];
		}
		else if(carta==1)
		{
			m[1] = m[2];
		}
	}
	public void pescaCarta(MazzoDiCarte mazzo)
	{
		m[2] = mazzo.pescaCarta();
	}
}