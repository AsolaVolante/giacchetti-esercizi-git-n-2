package estate;

import java.util.Scanner;

public class TrovaOrdina {
	
	public static void main(String[] args) {	

		Scanner in = new Scanner(System.in);
		System.out.println("Quanti numeri?");
		int x = in.nextInt();
		int vet[] = new int [x];
		
		for (int i=0; i<vet.length; i++){
			vet[i] = (int)(Math.random()*100);
		}

		
		System.out.println("Che numero vuoi che cerchi?");
		int trovato = in.nextInt();
		int i = 0;
		System.out.print("[");
		for (int j=0; j<vet.length; j++){
			System.out.print(vet[j]+" ");
		}
		System.out.print("]");
		System.out.println("");
		simpleSort(vet);
		System.out.print("[");
		for (int j=0; j<vet.length; j++){
			System.out.print(vet[j]+" ");
		}
		System.out.print("]");
		System.out.println("");
		
		System.out.print(trova(trovato, vet,i));
		in.close();
	}

	private static String trova(int trovato, int [] vet, int posizione) {
		//System.out.println(trovato+" "+vet+" "+posizione);
		if(posizione == vet.length) return "non trovato";
		if ( trovato == vet[posizione])
		return "Trovato: si trova alla posizione "+  posizione +" dell'array";		
		return trova(trovato, vet, ++posizione);
	}
	
	private static void simpleSort(int [] vet){
		for(int i=0; i<vet.length; i++){
			for(int j=i+1; j<vet.length; j++){
				if(vet[i] > vet[j]){
					int c;
					c = vet[i];
					vet[i] = vet[j];
					vet[j] = c;
				}
			}
		}
	}
	
	
	
}
