package scuolaGui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class GestoreClassiGUIGiacchetti {
	
	private Vector<Classe> CLASSI;
	  
	private JFrame frame;
	
	public GestoreClassiGUIGiacchetti(){
		  
	    CLASSI = new Vector<Classe>();
	    
	    frame = new JFrame("Classi Gui");
	    
	    JPanel southPanel = new JPanel();
	    JPanel centerPanel = new JPanel();
	    
	    JButton addButton = new JButton("ADD");
	    JButton cancelButton = new JButton("CANCEL");
	    JButton saveButton = new JButton("SAVE AND EXIT");
	    
	    JLabel annoLabel = new JLabel("ANNO");
	    JLabel sezioneLabel = new JLabel("SEZIONE");
	    JLabel indirizzoLabel = new JLabel("INDIRIZZO");
	    
	    JTextField annoField = new JTextField("", 1);
	    JTextField sezioneField = new JTextField("",1);
	    JTextField indirizzoField = new JTextField("",10);
	    
	    Box annoBox = Box.createHorizontalBox();
	    Box sezioneBox = Box.createHorizontalBox();
	    Box indirizzoBox = Box.createHorizontalBox();
	    Box theBigBox = Box.createVerticalBox();
	    
	    frame.getContentPane().add(southPanel, "South");
	    
	    southPanel.add(addButton);
	    southPanel.add(cancelButton);
	    southPanel.add(saveButton);

	    theBigBox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
	  
	    annoBox.add(annoLabel);
	    annoBox.add(Box.createRigidArea(new Dimension(8, 0)));
	    annoBox.add(annoField);
	    
	    sezioneBox.add(sezioneLabel);
	    sezioneBox.add(Box.createRigidArea(new Dimension(8, 0)));
	    sezioneBox.add(sezioneField);
	    
	    indirizzoBox.add(indirizzoLabel);
	    indirizzoBox.add(Box.createRigidArea(new Dimension(8, 0)));
	    indirizzoBox.add(indirizzoField);
	    
	    theBigBox.add(annoBox);
	    theBigBox.add(Box.createRigidArea(new Dimension(0, 10)));
	    theBigBox.add(sezioneBox);
	    theBigBox.add(Box.createRigidArea(new Dimension(0, 10)));
	    theBigBox.add(indirizzoBox);
	    
	    centerPanel.add(theBigBox);
	    
	    frame.getContentPane().add(centerPanel);
	    frame.setBounds(450, 280, 350, 250);
	    frame.setDefaultCloseOperation(3);
	    frame.setVisible(true);
	    
	    //Azioni del bottone che cancella
	    cancelButton.addActionListener(new ActionListener(){
	
	      public void actionPerformed(ActionEvent e) {
	        annoField.setText("");
	        sezioneField.setText("");
	        indirizzoField.setText("");
	      }
	      
	
	    });
	    //Azioni del bottone che aggiunge
	    addButton.addActionListener(new ActionListener() {
	     
	    	public void actionPerformed(ActionEvent e) {
	    		
	    		String anno = annoField.getText();
	  	    	String sezione = sezioneField.getText();
	    		String indirizzo = indirizzoField.getText();
	      
		        CLASSI.add(new Classe(Integer.parseInt(anno),sezione.charAt(0), indirizzo));
		        
		        annoField.setText("");
		        sezioneField.setText("");
		        indirizzoField.setText("");
	      }
	      
	    });

	    //Azioni del bottone che salva
	    saveButton.addActionListener(new ActionListener()  {
	
	      public void actionPerformed(ActionEvent e){
	        try{
		          FileOutputStream out = new FileOutputStream("ListaClassi.dat");
		          ObjectOutputStream outputStream = new ObjectOutputStream(out);
		          //sta roba l' ho trovata in internet
		          outputStream.writeObject(CLASSI);
		          outputStream.flush();
		          outputStream.close();
	        }
	      
	        catch (IOException Errori) {
	          Errori.printStackTrace();
	        }
	        
	        frame.dispose();
	      }
	    });
	}

	public static void main(String[] args) {
	 
		new GestoreClassiGUIGiacchetti();
	}
}