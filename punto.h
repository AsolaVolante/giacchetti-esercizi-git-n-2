/*struct punto_s{
	double x,//ascissa
		   y;//ordinata
};
typedef struct punto_s * Punto;
*/
typedef struct punto_s{
	double x,y;
}* Punto;

Punto newPunto(double,double);

Punto newPunto0();

void destroyPunto(Punto*);

void printPunto(Punto);

double distanza(Punto,Punto);

double distanza0(Punto);

void trasla(double,double);

double getx(Punto);
double gety(Punto);
double rho(Punto);
double alfa(Punto);
