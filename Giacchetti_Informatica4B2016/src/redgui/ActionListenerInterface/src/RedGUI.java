package redgui.ActionListenerInterface.src;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
//ma dove gli diciamo di cambiare colore?
public class RedGUI {
	
	private JFrame frame;

	public RedGUI() {
		super();
		frame = new JFrame("RedGUI ActionListenerInterface");
		JButton button = new JButton("OK");
		frame.add(button, BorderLayout.SOUTH);
		button.addActionListener(new OutsideListener(frame));
		frame.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new RedGUI();
				
			}
		});
	}
}

