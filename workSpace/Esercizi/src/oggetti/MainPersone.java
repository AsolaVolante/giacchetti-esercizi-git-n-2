package oggetti;

import java.util.Scanner;

public class MainPersone {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		int risposta;
		
		boolean esciCiclo = false;
		
		do{	
			Menu();

			risposta = in.nextInt();

			switch(risposta){
				
			case 1:
				Studentee Bullo = new Studentee("Pirlotto","Uxoricidi", "Belluno, via Tasso 31", 123445, "ITIS");
				Bullo.si_presenta();
				Bullo.cambiocasa("via rotule cadute in guerra","Scientifico");
				Bullo.si_presenta();
				break;
			
			case 2:
				Lavoratore Prof = new Lavoratore("GianFranchio","Ammazzaneonati", "Belluno, via caduti a Napoli 13",400, "ITIS");
				Prof.si_presenta();
				Prof.cambiocasa("''Villa Mortadella Ammuffita''");
				Prof.si_presenta();
				Prof.cambiocasa("Via Sposato col Cani","Renier");
				Prof.si_presenta();
				break;
			
			case 3:
				Persona Cittadino = new Persona("Fabio","Onanista", "Belluno, via le mani dal naso 39");
				Cittadino.si_presenta();
				Cittadino.cambiocasa("via da qui");
				Cittadino.si_presenta();
				break;
			
			case 0:
				esciCiclo = true;
				break;
			
			default:
				System.out.println("La prossima volta inserisci un numero del menu...");
				break;
			
			}	
	
		}while(!esciCiclo);
	
	in.close();
	
	}

	private static void Menu() {
		System.out.println("");
		System.out.println("scegli:");
		System.out.println("        1. Studente");
		System.out.println("        2. Lavoratore");
		System.out.println("        3. Persona Random");
		System.out.println("        0. esci");
	
	}
}