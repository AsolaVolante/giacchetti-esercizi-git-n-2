package util;

public class Array {

	public static void sort(Comparable[] comparable){
		
		int pos_min, i, j;
		util.Comparable temp;
			
		for(i=0; i<comparable.length-1; i++) {
			pos_min=i;
			for(j=i+1; j<comparable.length; j++)
				if(comparable[pos_min].compareTo(comparable[j])>0)
					pos_min=j;
			temp=comparable[i];
			comparable[i]=comparable[pos_min];
			comparable[pos_min]=temp;	
		}
			
	}
}
