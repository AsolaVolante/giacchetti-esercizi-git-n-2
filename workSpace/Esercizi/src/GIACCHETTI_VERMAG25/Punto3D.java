package GIACCHETTI_VERMAG25;

public class Punto3D extends Pixel {
	
	protected float z;
	
	public Punto3D(float x, float y,String colore, float z) {
		super(x, y,colore);
		this.z = z;
	}
	
	//ovrerride di "toString" che si trova in "Pixel"
	public String toString() {
		return "x = "+x+" , y = "+y+" , z = "+z+" color "+colore;
	}
	
	public float getZ() {
		return z;
	}
	
	public void setZ(float z) {
		this.z = z;
	}
	
	public void equals(Punto3D B){
		if (this.getX() == B.getX()){
			if(this.getY() == B.getY()){
				if (this.getColore() == B.getColore()){
					if (this.getZ() == B.getZ()){
						System.out.print("i punti3D sono uguali");
					}
				}
			}
		}
		else System.out.print("i Punti3D sono diversi");
	}
}
