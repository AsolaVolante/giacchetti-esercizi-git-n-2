package guiSegreteria;

import java.io.Serializable;

public class Alunno implements Serializable{
	
	private String nome, cognome;
	private Classe classe;
	
	/**
	 * @param Nome : tipo Gianni
	 * @param Cognome : tipo Rossi
	 */
	public Alunno(String n, String c){
		this.nome=n;
		this.cognome=c;
	}
	
	public void setClasse(Classe c){
		this.classe=c;
	}
	public Classe getClasse(){
		return classe;
	}
	public String getNome(){
		return nome;
	}
	public String getCognome(){
		return cognome;
	}
	
	public String toString(){
		return nome+" "+cognome+" "+classe;
	}
}
