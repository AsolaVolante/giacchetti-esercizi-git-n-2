package redgui.ActionListenerInterface.src;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class GUI {
	
	class MyListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			System.out.println("hello world!");
			
		}
		
	}

	public GUI() {
		super();
		JFrame frame = new JFrame("project ActionListenerInterface");
		JButton button = new JButton("OK");
		frame.add(button);
		button.addActionListener(new MyListener());
		button.addActionListener(new MyListener());
		frame.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new GUI();
				
			}
		});
	}
}