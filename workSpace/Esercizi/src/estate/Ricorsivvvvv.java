package estate;

import java.util.Scanner;

public class Ricorsivvvvv {
	
	public static void main(String[] args) {	

		
		Scanner in = new Scanner(System.in);
		System.out.println("fibonacci di ?");
		int x = in.nextInt();
		System.out.println("fibonacci di "+ x +" = "+ fibonacci(x));
		System.out.println("fibonacci di "+ x +" = "+ fibonacci2(x));
		in.close();
	}
	
	public static int moltiplicazione(int a, int b){
		System.out.println("a = " + a + " b = " + b);
		if (b == 1) return a;
		if (b == 0) return 0;
		
		return a + moltiplicazione(a, b-1);
	}
	
	public static int fattoriale(int a){
	
		if (a == 0) return 1;
		else return a * fattoriale(a-1);
	}
	/*	F(i) = 0                 se i = 0
     	F(i) = 1                 se i = 1
     	F(i) = F(i-1) + F(i-2)   se i >= 2
	 */
	public static int fibonacci(int a){
		if (a == 0) return 1;
		if (a == 1) return 1;
		return a = fibonacci(a-1) + fibonacci(a-2);
	}
	
	public static int fibonacci2(int n){
        
		n++;
        
		if(n == 1 || n == 2) return 1;

        int fibo1 = 1, fibo2 = 1, fibonacci = 1;
        
        for(int i = 3; i <= n; i++){
           
        	fibonacci = fibo1 + fibo2; 
            fibo1 = fibo2;
            fibo2 = fibonacci;
 
        }
        return fibonacci; 
    }     
}
