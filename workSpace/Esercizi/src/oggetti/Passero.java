package oggetti;

public class Passero extends Uccello {

	public Passero(String nome, String movimento, int zampe, String propulsione, String verso) {
		super(nome);
		zampe = getZampe();
		movimento = getMovimento();
		propulsione = getPropulsione();
		this.verso = getVerso();
		buonanotte();
	}

	public String getVerso() {
		return verso;
	}
	
	public void buonanotte(){
		System.out.println("\"Buonanotte: Buonanotte, dormo sul mio ramo \" ");
	}
	protected String verso;
}
