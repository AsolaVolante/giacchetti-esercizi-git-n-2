package jPanel_Varie_Interfaccie;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class AbsoluteLayoutDemo {

	public static void main(String[] args) {
		
		JFrame j = new JFrame("Test");
		JLabel la = new JLabel("Label");
		la.setOpaque(true);
		la.setBackground(Color.CYAN);
		la.setBounds(100, 100, 200, 50);
		JButton b = new JButton("Button");
		b.setBounds(200, 500, 200, 50);
		j.setLayout(null);
		j.add(la);
		j.add(b);
		j.setVisible(true);
		j.setSize(200,200);
	}

}