package GIACCHETTI_VERMAG25;

public class Segmento {

	Punto a;
	Punto b;
	
	public Segmento(Punto a, Punto b) {	
		this.a = a;
		this.b = b;
		lunghezza(a,b);	
	}
	
	
	protected float lunghezza(Punto puntoA, Punto puntoB){
		float lunghezza = (float) Math.sqrt(Math.pow((puntoA.getX() - puntoB.getX()), 2) + Math.pow((puntoA.getY() - puntoB.getY()), 2));
		return lunghezza;
	}
	
	public String toString() {
		return "lunghezza = "+lunghezza(a,b);
	}
}
