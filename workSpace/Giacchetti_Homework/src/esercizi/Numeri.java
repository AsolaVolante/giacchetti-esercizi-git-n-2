package esercizi;

public class Numeri implements Comparable {

	int numero;
	
	public Numeri(int numero) {
		this.numero = numero;
	}
	
	public int getNumero() {
		return numero;
	}

	@Override
	public int compareTo(Comparable o) {

		return this.numero-((Numeri)o).numero;
	}

}
