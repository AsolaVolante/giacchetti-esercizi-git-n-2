package oggetti;

public class Carta {
	
	protected int numero;
	protected String seme;
	
	public Carta()
	{
	
	}
	
	public Carta(int numero, String seme)
	{
		this.numero = numero;
		this.seme = seme;
	}
	
	public void visualizzaCarta()
	{
		System.out.println(numero + " di " + seme);
	}
}