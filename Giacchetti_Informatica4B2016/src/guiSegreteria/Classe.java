package guiSegreteria;

import java.io.Serializable;
/**	
 *@author giacchettig
 */

/**
 *@Descrizione l' oggetto che descrive la Classe 
 */
public class Classe
implements Serializable {
    private int anno;
    private char sezione;
    private String indirizzo;
    /**
	 * @param Anno : dal 1 al 5 anno (un carattere)
	 * @param Sezione : A,B,C... (un carattere)
	 * @param Indirizzo : Informatica,Meccanica... (una Stringa)
	 */
    public Classe(int anno, char sezione, String indirizzo) {
        this.anno = anno;
        this.sezione = sezione;
        this.indirizzo = indirizzo;
    }

    public int getAnno() {
        return this.anno;
    }

    public char getSezione() {
        return this.sezione;
    }

    public String getIndirizzo() {
        return this.indirizzo;
    }
	/**
	 *@Descrizione 
	 *il metodo toString ritorna la classe sotto forma di stringa 
	 *il "substring" prende i caratteri della stringa in queestione compresi tra i 2 parametri
	 */
    public String toString() {
        return "" + this.anno + this.sezione + this.indirizzo.substring(0, 3);
    }
}

