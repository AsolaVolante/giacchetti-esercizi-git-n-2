package guiSegreteria;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class CreaClassi {
    private JFrame frame = new JFrame("ClassiInputGUI2");
    private Integer[] anni = new Integer[]{1, 2, 3, 4, 5};
    private Character[] sezioni = new Character[]{'A', 'B','C','D','E','F','G','H','I','L','M','N','O','P','Q','R','S','T','U','V','Z'};
    private String[] indirizzi = new String[]{"Informatica", "Elettronica", "Elettrotecnica",  "Meccanica","Energia","Edile"};
    private JTextArea ar= new JTextArea();
    private Dati dati;
    
    public CreaClassi() {
    	dati = readDati();
        JPanel southPanel = new JPanel();
        JButton addButton = new JButton("ADD");
        JButton cancelButton = new JButton("CANCEL");
        JButton saveButton = new JButton("SAVE");
        JButton stampButton = new JButton("STAMP");
        southPanel.add(addButton);
        southPanel.add(cancelButton);
        southPanel.add(saveButton);
        southPanel.add(stampButton);
        this.frame.getContentPane().add((Component)southPanel, "South");
        JPanel centerPanel = new JPanel();
        Box centerBox = Box.createVerticalBox();
        centerBox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        Box annoBox = Box.createHorizontalBox();
        Box sezioneBox = Box.createHorizontalBox();
        Box indirizzoBox = Box.createHorizontalBox();
        JLabel annoLabel = new JLabel("ANNO");
        final JComboBox<Integer> annoComboBox = new JComboBox<Integer>(this.anni);
        annoComboBox.setSelectedIndex(-1);
        annoBox.add(annoLabel);
        annoBox.add(Box.createRigidArea(new Dimension(10, 0)));
        annoBox.add(annoComboBox);
        JLabel sezioneLabel = new JLabel("SEZIONE");
        final JComboBox<Character> sezioneComboBox = new JComboBox<Character>(this.sezioni);
        sezioneComboBox.setSelectedIndex(-1);
        sezioneBox.add(sezioneLabel);
        sezioneBox.add(Box.createRigidArea(new Dimension(10, 0)));
        sezioneBox.add(sezioneComboBox);
        JLabel indirizzoLabel = new JLabel("INDIRIZZO");
        final JComboBox<String> indirizzoComboBox = new JComboBox<String>(this.indirizzi);
        indirizzoComboBox.setSelectedIndex(-1);
        indirizzoBox.add(indirizzoLabel);
        indirizzoBox.add(Box.createRigidArea(new Dimension(10, 0)));
        indirizzoBox.add(indirizzoComboBox);
        centerBox.add(annoBox);
        centerBox.add(Box.createRigidArea(new Dimension(0, 10)));
        centerBox.add(sezioneBox);
        centerBox.add(Box.createRigidArea(new Dimension(0, 10)));
        centerBox.add(indirizzoBox);
        centerPanel.setLayout(new GridLayout(2, 1));
        centerPanel.add(centerBox);
        centerPanel.add(ar);
        this.frame.getContentPane().add(centerPanel);
        this.frame.setBounds(400, 200, 350, 300);
        this.frame.setDefaultCloseOperation(3);
        this.frame.setVisible(true);
        
        addButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer anno = (Integer)annoComboBox.getSelectedItem();
                if (anno == null) {
                    return;
                }
                Character sezione = (Character)sezioneComboBox.getSelectedItem();
                if (sezione == null) {
                    return;
                }
                String indirizzo = (String)indirizzoComboBox.getSelectedItem();
                if (indirizzo == null) {
                    return;
                }
                CreaClassi.this.dati.add(new Classe(anno, sezione.charValue(), indirizzo));
                annoComboBox.setSelectedIndex(-1);
                sezioneComboBox.setSelectedIndex(-1);
                indirizzoComboBox.setSelectedIndex(-1);
            }
        });
        
        cancelButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                annoComboBox.setSelectedIndex(-1);
                sezioneComboBox.setSelectedIndex(-1);
                indirizzoComboBox.setSelectedIndex(-1);
                
            }
        });
        
        saveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FileOutputStream out = new FileOutputStream("F:/workSpace/GuiScuola/src/guiSegreteria/dati.dat");
                    ObjectOutputStream outputStream = new ObjectOutputStream(out);
                    outputStream.writeObject(dati);
                    outputStream.flush();
                    outputStream.close();
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
           //     ClassiInputGUI2.this.frame.dispose();
            }
        });
        
        stampButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e) {
        		FileOutputStream out;
				try {
					
					 ObjectInputStream objectInputStream = null;
				        FileInputStream fileInputStream = null;
				        fileInputStream = new FileInputStream("F:/workSpace/GuiScuola/src/guiSegreteria/dati.dat");
				        objectInputStream = new ObjectInputStream(fileInputStream);
				        Dati dat = (Dati)objectInputStream.readObject();
 			            ar.setText(dat.toStringC());
				} 
				catch (IOException e1) {
                    e1.printStackTrace();
                
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
                
        	}
        });
    }
    public Dati readDati() {
		
		ObjectInputStream objectInputStream = null;
		FileInputStream fileInputStream = null;
		Dati dati = null;
		
		try {
			fileInputStream = new FileInputStream("F:/workSpace/GuiScuola/src/guiSegreteria/dati.dat");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		try {
			objectInputStream = new ObjectInputStream(fileInputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {

			dati =  (Dati) objectInputStream.readObject();
			System.out.println(dati);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		try {
			objectInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dati;
	}

    public static void main(String[] args) {
        new CreaClassi();
    }

}

