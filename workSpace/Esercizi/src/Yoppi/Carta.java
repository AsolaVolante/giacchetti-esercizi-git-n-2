package Yoppi;

public class Carta{
	
   
	private int numero;
   
	private String seme;
    public Carta()
    {}
    
    public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Carta(int num,String seme)
    {
        numero = num;
        this.seme = seme;
    }
    public void visualizzaCarta()
    {
        System.out.println("Numero carta = "+numero+" � il seme � "+seme);
    }
    
    public String getSeme() {
		return seme;
	}
    
    public void setSeme(String seme) {
		this.seme = seme;
	}
}