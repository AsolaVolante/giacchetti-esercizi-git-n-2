package esercizi;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class VerticiLayout implements LayoutManager {

	Component component[] = new Component[4];
	
	@Override
	public void addLayoutComponent(String arg0, Component arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void layoutContainer(Container parent) {
		int x=parent.getWidth(), y=parent.getHeight();
		if(parent.getComponentCount()<=4)
			component=parent.getComponents();
		else
			for(int i=0; i<=4; i++)
				component[i]=parent.getComponent(parent.getComponentCount()-i);
		component[0].setBounds(0, 0, x/10, y/10);
		component[1].setBounds(x-(x/10), 0, x/10, y/10);
		component[2].setBounds(x-(x/10), y-(y/10), x/10, y/10);
		component[3].setBounds(0, y-(y/10), x/10, y/10);		
	}

	@Override
	public Dimension minimumLayoutSize(Container arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension preferredLayoutSize(Container arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeLayoutComponent(Component arg0) {
		// TODO Auto-generated method stub

	}

}
