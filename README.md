# OBBIETTIVI#
Elenco azioni svolte ogni lezione dal 30/1

### Lezione 30/1/2017 : 50' ###

* ho spiegato al bocia qualcosa sull' informatica di base. spero che abbia capito
* Ho cercato di capire di più l' interfaccia grafica della segreteria, per preparami a spiegarla. devo studiarla anocora molto
* [lezione lunedì 30](https://trello.com/c/IOfmIOyl/71-lunedi-30)


### Lezione 2/2/2017 : 120' ###

* Mettere l'esito all' alunno è più difficile di quanto pensassi
* Studiare il Card Lay (ci sono stato 10 minuti e non è servito molto)


### Lezione 4/2/2017 : 60' ###
* Cerco di  capire il Card Layuout :smile:

### Lezione 6/2/2017 : 50' ###

* ho scoperto il TabbedPanel, abbastanza intuitivo. 
* ho ripassato ciò che dovevo dire all' interrogazione
* problemi tecnici dovuti alla mancanza di chiavetta

### Lezione 9/2/2017 : 120' ###
* [immagine input-output stream](https://trello-attachments.s3.amazonaws.com/57e5248e6ab2d1e0c41130f9/584e6c431037b1edd5169fab/18d4d65b5de3c7c1ecca152054cb734a/serial-ATP.png) ho usato questa immagine per ripassare la questione Stream.
* sono stato interrogato
* ho messo l' esito con cardLayout (fallito), e senza.
* provo a mettere tutto assieme con tabbed layout.