package oggetti;

public class Cilindro extends Cerchio{
	
	private double altezza;
	
	public double getAltezza() {
		return altezza;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}

	public Cilindro (double raggio, double altezza){
		super(raggio);
		this.altezza = altezza;
	}
	
	public double volume(){
		double vol = area()*altezza;
		return vol;
	}
}
