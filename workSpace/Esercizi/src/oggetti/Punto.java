package oggetti;

class Punto {
	
	private float x,y;

	/**
	 * @param x coordinata del punto
	 * @param y coordinata del punto
	 */
	public Punto(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public float getX(){
		return x;
	}

	public float getY() {
		return y;
	}

	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}
}