package GIACCHETTI_VERMAG25;

public class Pixel extends Punto {

	protected String colore;
	
	public Pixel(float x, float y, String colore) {
		super(x, y);
		this.colore = colore;
	}
	
	public String getColore() {
		return colore;
	}
	
	public void setColore(String colore) {
		this.colore = colore;
	}
	
	//ovrerride di "toString" che si trova in "Punto"
	public String toString() {
	return "x = "+x+" , y = "+y+" color "+colore;
	}
	
	public void equals(Pixel B){
		if (this.getX() == B.getX()){
			if(this.getY() == B.getY()){
				if (this.getColore() == B.getColore()){
					System.out.print("i pixel sono uguali");
				}
			}
		}
		else System.out.print("i pixel sono diversi");
	}
}
