package oggetti;
/*Creare una sottoclasse di Persona, Lavoratore, che ha anche stipendio e azienda. Anche Lavoratore si presenta
e cambiocasa cambiando anche azienda oppure cambiocasa cambiando solo casa. Gestire nel main entrambi i casi.
*/
public class Lavoratore extends Persona {

	protected int stipendio;
	protected String azienda;
	
	public Lavoratore(String nome, String cognome, String indirizzo, int stipendio,String azienda) {
		super(nome, cognome, indirizzo);
		this.azienda = azienda;
		this.stipendio = stipendio;
	}
	
	public void si_presenta(){
		super.si_presenta();
		System.out.println("Frequento l' istituto "+ azienda +" e il mio stipendip �: "+ stipendio);
	}
	
	public void cambiocasa(String nuovoIndirizzo, String azienda){
		super.cambiocasa(nuovoIndirizzo);
		this.azienda = azienda;
	}
}
