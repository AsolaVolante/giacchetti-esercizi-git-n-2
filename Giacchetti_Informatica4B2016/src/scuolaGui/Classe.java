package scuolaGui;

import java.io.Serializable;

public class Classe implements Serializable{
	
	private int anno;
	private char sezione;
	private String indirizzo;
	
	// TODO Auto-generated method stub
	public Classe(int anno, char sezione, String indirizzo) {
		super();
		this.anno = anno;
		this.sezione = sezione;
		this.indirizzo = indirizzo;
	}
	
	// TODO Auto-generated method stub
	public int getAnno() {
		return anno;
	}
	// TODO Auto-generated method stub
	public char getSezione() {
		return sezione;
	}
	// TODO Auto-generated method stub
	public String getIndirizzo() {
		return indirizzo;
	}
	
	@Override
	public String toString() {
		return "Classe: Anno Scolastico = " + anno + ", Sezione = " + sezione + ", Indirizzo = " + indirizzo;
	}
	

}
