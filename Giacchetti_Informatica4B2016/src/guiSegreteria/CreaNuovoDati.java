package guiSegreteria;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
/**
 *@Descrizione 
 *Serve a creare un NUOVO file DATI.dat
 *aggiorna e sostituisce il vecchio dati.dat
 */
public class CreaNuovoDati {
	
	public static void creazione(Dati dati){
		try {
            FileOutputStream out = new FileOutputStream("F:/workSpace/GuiScuola/src/guiSegreteria/dati.dat");
            ObjectOutputStream outputStream = new ObjectOutputStream(out);
            outputStream.writeObject(dati);
            outputStream.flush();
            outputStream.close();
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
	}
	/**
	 *@Descrizione 
	 *Infatti fa riferimento a Dati da qui, dal main
	 */
	public static void main(String[] args) {
		
		Dati dati = new Dati();
		
		creazione(dati);
		
	}

}
