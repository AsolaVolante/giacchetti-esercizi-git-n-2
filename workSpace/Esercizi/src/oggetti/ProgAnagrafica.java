package oggetti;

import java.util.Scanner;

public class ProgAnagrafica {

	public static void main(String[] args) {

		Scanner s = new Scanner (System.in);

		//dati di anagrafica
		
		String email;
		Anagrafica contatto = new Anagrafica();
		
		System.out.println("Inserisci il nome: ");
		try
		{
			contatto.nome = s.nextLine();
		}
		catch(Exception e){}
	
		System.out.println("Inserisci il cognome: ");
		try
		{
			contatto.cognome = s.nextLine();
		}
		catch(Exception e){}
		
		System.out.println("Inserisci l'email: ");
		try
		{
			email=s.nextLine();
			contatto.registraEmail(email);
		}
		catch(Exception e){}
		
		System.out.println("RIEPILOGO CONTATTO:");
		contatto.stampaDati();

		System.out.print("\n----------------------------------------------------------------------------------------------\n");	
		
		//dati di uno studente
		Studente studente = new Studente();
		
		System.out.println("Inserisci la matricola: ");
		try
		{			
			studente.matricola = s.nextLine();
		}
		catch(Exception e){}
		
		System.out.println("RIEPILOGO CONTATTO:");
		studente.stampaDati();
		
		System.out.print("\n----------------------------------------------------------------------------------------------\n");

		//dati di un dipendente
		Dipendente dipendente = new Dipendente();
		// dati quando il livello � 1 (livellobase)
		System.out.println("Livello: " + dipendente.getLevel());
		System.out.println("Stipendio: " + dipendente.getStipendio());
		
		dipendente.setLevel(4);
		
		System.out.println("Livello: " + dipendente.getLevel());
		System.out.println("Stipendio: " + dipendente.getStipendio());
		
		dipendente.setLevel(2);
		
		System.out.println("Livello: " + dipendente.getLevel());
		System.out.println("Stipendio: " + dipendente.getStipendio());

		System.out.print("\n----------------------------------------------------------------------------------------------\n");
/*

 
		//dati di un dipendente con bonus
		BonusDipendente stipendiato = new BonusDipendente();
		
		stipendiato.setLivello(3);
		
		System.out.println("Livello: " + stipendiato.getLivello());
		System.out.println("Stipendio: " + stipendiato.getStipendio());
		
		stipendiato.setLivello(5);
		
		System.out.println("Livello: " + stipendiato.getLivello());
		System.out.println("Stipendio: " + stipendiato.getStipendio());
		
		stipendiato.setLivello(3);
		
		System.out.println("Livello: " + stipendiato.getLivello());
		System.out.println("Stipendio: " + stipendiato.getStipendio());
		
		stipendiato.setLivello(2);
		
		System.out.println("Livello: " + stipendiato.getLivello());
		System.out.println("Stipendio: " + stipendiato.getStipendio());
		
	}
	*/
}
}