package estate;

  class Radiosveglia {
	 
	 byte ora;
	 byte minuti;
	 byte secondi;
	
	protected byte getOra() {
		return ora;
	}
	
	 protected void setOra(byte ora) {
		this.ora = (byte)(Math.random()*24);
	}
	
	 public byte getMinuti() {
		return minuti;
	}
	
	 public void setMinuti(byte minuti) {
		this.minuti = (byte)(Math.random()*60);
	}
	
	 public byte getSecondi() {
		return secondi;
	}
	
	 public void setSecondi(byte secondi) {
		this.secondi = (byte)(Math.random()*60);
	}
	 
}
