package oggetti;
import java.util.Scanner;

public class ProgCont {
	
	public static void main(String argv[]){
	
		Scanner in = new Scanner(System.in);
		
		ContoSpeciale conto =new ContoSpeciale();
		Assegno versamento=new Assegno();
		String valore ="";
		
		double importo=0;
		
		System.out.print("Importo dell'assegno: ");
		valore =in.nextLine();
		versamento.importo=Double.valueOf(valore).doubleValue();
		conto.versa(versamento);
		conto.stampaSaldo();
		
		System.out.print("Importo da prelevare: ");
		valore=in.nextLine();
		importo =Double.valueOf(valore).doubleValue();
		conto.preleva(importo);
		conto.stampaSaldo();
	}
}
