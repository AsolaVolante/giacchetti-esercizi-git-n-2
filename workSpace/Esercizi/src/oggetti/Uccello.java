package oggetti;

public class Uccello extends Animale {
	
	public Uccello(String nome) {
		super(nome);
		this.zampe = getZampe();
		this.movimento = getMovimento();
		this.propulsione = getPropulsione();
	}
	
	protected int zampe;
	protected String movimento;
	protected String propulsione;
	
	public int getZampe() {
		return zampe;
	}
	public String getMovimento() {
		return movimento;
	}
	public String getPropulsione() {
		return propulsione;
	}

	public void siPresenta(){
		System.out.println("Un uccello si presenta: ");
		System.out.println("Sono un uccello, mi chiamo " + getNome() + " e ho " + getZampe() + " zampe , " + getMovimento() + " con le mie " + getPropulsione());
	}
}
