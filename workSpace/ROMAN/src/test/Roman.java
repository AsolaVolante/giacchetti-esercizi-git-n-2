package test;

import java.util.HashMap;
import java.util.Map;

public class Roman {
	private static final Map<Integer,String> map = new HashMap<Integer,String>();
	static{
		map.put(40,"XL");
		map.put(10,"X");
		map.put(9,"IX");
		map.put(5,"V");
		map.put(4,"IV");
		map.put(1,"I");
		
		
	}
	private int decimal;

	public Roman(int i){
		decimal = i;
	}
	
	@Override
	public String toString() {
		if(map.containsKey(decimal))
		return map.get(decimal);
		int[] keys = new int[] {40,10,9,5,4,1};
		for (int key : keys) {
			if(decimal>key)
				return new Roman(key).toString() + new Roman(decimal-key).toString();
		}
		return "";
	}
}
