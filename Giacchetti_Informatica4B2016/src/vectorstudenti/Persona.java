package vectorstudenti;
/*
 * Creare classe Persona con attributi nome, cognome, indirizzo.
Creare sottoclasse Studente con attributo scuola e matricola. Creare gli opportuni costruttori e metodi get e set.
Persona e Studente hanno il metodo si_presenta() che stampa tutti gli attributi organizzati in una frase del tipo
 "mi chiamo... ", crearlo anche in Studente tramite override.
Persona ha metodo cambiocasa(nuovoindirizzo), Studente fa overloading di questo metodo per cambiare anche scuola.
Nel main creare una Persona e uno Studente, farli presentare , cambiare casa a entrambi e farli ripresentare.
Creare una sottoclasse di Persona, Lavoratore, che ha anche stipendio e azienda. Anche Lavoratore si presenta
 e cambiocasa cambiando anche azienda oppure cambiocasa cambiando solo casa. Gestire nel main entrambi i casi.
 */

//OVERRIDING; tra 2 classi, di un metodo c'� lo stesso nome e stesso parametro
//OVERLOADING;	in una classe ci sono 2 metodi con stesso nome e parametri diversi
public class Persona {
	
	protected String nome;
	protected String cognome;
	protected String indirizzo;
	
	public Persona (String nome,String cognome,String indirizzo){
		this.nome = nome;
		this.cognome = cognome;
		this.indirizzo = indirizzo;
	}
	
	protected void si_presenta(){
		System.out.println("Mi chiamo "+ nome +" "+ cognome +" e vivo in "+ indirizzo);
	}
	
	protected void cambiocasa(String nuovoIndirizzo){
		this.indirizzo = nuovoIndirizzo;
	}
}
