package esercizi;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class SwingDemo {
	
	public SwingDemo(){	
		JFrame frame = new JFrame("Un semplice programma swing");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		
		System.out.println("Eseguito");
		
		//crea il frame sul event displathing thread
		SwingUtilities.invokeLater(new Runnable(){
			
			@Override
			public void run(){
				
				new SwingDemo();
				
			}
	
		});
		System.out.println("Eseguito");
	}
	
	ActionListener a;
	JButton b;
}