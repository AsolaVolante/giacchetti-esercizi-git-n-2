package oggetti;

public class Pesce extends Animale {

	public Pesce( String nome, String movimento, String propulsione) {
		super(nome);
		this.movimento = getMovimento();
		this.propulsione = getPropulsione();
	}
	
	protected String movimento ;
	protected String propulsione;
	

	public String getMovimento() {
		return movimento;
	}
	public String getPropulsione() {
		return propulsione;
	}
	
	public void siPresenta(){
		System.out.println("Un pesce si presenta: ");
		System.out.println("Sono un pesce, mi chiamo " + getNome() + ", " + getMovimento() + " con le mie " + getPropulsione());
	}
}
