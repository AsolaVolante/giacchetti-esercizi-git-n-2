package jPanel_Varie_Interfaccie;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;

public class CircleLayoutDemo extends JFrame{

		
		Container c =getContentPane();
		JButton b = new JButton("1");
		JButton b2 = new JButton("2");
		JButton b3 = new JButton("3");
		JButton b4 = new JButton("4");
		JButton b5 = new JButton("5");
		JButton b6 = new JButton("6");
		
		public CircleLayoutDemo() {
			super("CircleLayoutDemo");
			
			c.setLayout(new CircleLayout());
			c.add(b);
			c.add(b2);
			c.add(b3);
			c.add(b4);
			c.add(b5);
			c.add(b6);
			
			setSize(400,400);
			setVisible(true);
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		}

		public static void main(String[] args) {
			CircleLayoutDemo cld = new CircleLayoutDemo();

		}


}
