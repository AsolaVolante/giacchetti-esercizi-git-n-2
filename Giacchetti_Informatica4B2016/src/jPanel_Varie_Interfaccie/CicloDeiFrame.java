package jPanel_Varie_Interfaccie;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class CicloDeiFrame {
	
	public static void main(String[]args){
	
		 
		JLabel  label = new JLabel ("Yolla Ciolla di 20 cm");
		
		SwingUtilities.invokeLater(new Runnable(){	
		
			//int ingrandisce = 1;
			@Override
			public void run(){
				
				for (int i = 0; i < 60; i++) {
			
					//ingrandisce = ingrandisce * 2;
					JFrame frame = new JFrame(i+". Un semplice programma swing ");
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setBounds((int)(Math.random()*850),(int)(Math.random()*850), 100, 100);
					frame.setSize(i,i);
					frame.getContentPane().add(label);
					frame.setVisible(true);
				}
				
			}
	
		});
		
	}
}

