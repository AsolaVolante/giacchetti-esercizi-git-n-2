package scuolaGui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class SalvaDaTastiDemo extends JFrame{
	
	SalvaDaTastiPanel panel = new SalvaDaTastiPanel("","","");
	
	Container container = getContentPane();
	//il panel coef � tutta la colonna a destra
	JPanel ibottoni = new JPanel();
	JPanel leLabel = new JPanel();
	JPanel panelAnno = new JPanel();
	JPanel panelSezione = new JPanel();
	JPanel panelIndirizzo = new JPanel();

	JLabel labelAnno = new JLabel("ANNO ");
	JLabel labelSezione = new JLabel("SEZIONE ");
	JLabel labelIndirizzo = new JLabel("INDIRIZZO ");
	
	JTextField anno = new JTextField("", 5);
	JTextField sezione = new JTextField("" ,3);
	JTextField ind = new JTextField("" ,10);
	
	JButton salva = new JButton("Salva");
	JButton  cancella = new JButton("Cancella");
	
	public SalvaDaTastiDemo() {
		super("Inseritore");
		container.setLayout(new BorderLayout());
		leLabel.setLayout(new GridLayout(3, 1));
		panelAnno.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelSezione.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelIndirizzo.setLayout(new FlowLayout(FlowLayout.CENTER));
		anno.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panel.setA(anno.getText());
			}
		});
		sezione.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panel.setB(sezione.getText());
			}
		});
		ind.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panel.setC(ind.getText());
			}
		});
		
		panelAnno.add(labelAnno);
		panelAnno.add(anno);
		panelSezione.add(labelSezione);
		panelSezione.add(sezione);
		panelIndirizzo.add(labelIndirizzo);
		panelIndirizzo.add(ind);
		
		leLabel.add(panelAnno);
		leLabel.add(panelSezione);
		leLabel.add(panelIndirizzo);

 
 		ibottoni.add(salva);
 		ibottoni.add(cancella);
		container.add(ibottoni);
		container.add(ibottoni, BorderLayout.SOUTH);
		container.add(leLabel, BorderLayout.NORTH);
		setVisible(true);
		setSize(300, 200);
		
		salva.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				System.out.print("Anno = " + anno.getText()+"\n");
				System.out.print("Sezione = " + sezione.getText()+"\n");
				System.out.print("Indirizzo = " + ind.getText()+"\n");
				
			}
		});
		
		cancella.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				anno.setText("");
				sezione.setText("");
				ind.setText("");
				
			}
		});
	
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new SalvaDaTastiDemo();
				
			}
		});
		
	}	
}
