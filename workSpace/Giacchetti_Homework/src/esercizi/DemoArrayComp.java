package esercizi;

import util.Array;

public class DemoArrayComp {

	public static void main(String[] args) {

		util.Comparable a = new Numeri(4);
		util.Comparable b = new Numeri(3);
		util.Comparable c = new Numeri(2);
		util.Comparable d = new Numeri(1);
		
		util.Comparable[] array = {a,b,c,d};
		
		Array.sort(array);
		
		for(int i=0; i<4; i++)
		{
			System.out.println(((Numeri)array[i]).numero);
		}
		
	}

}
